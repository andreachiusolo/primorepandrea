package it.begear.fantacollcetion.controller;

import it.begear.fantacollcetion.repository.FantaLista;
import it.begear.fantacollcetion.util.ScannerManager;
import it.begear.fantacollection.exc.MiaEccezione;
import it.begear.fantacollection.view.Menu;

public class FantaCollection {

	public static void main(String[] args) {

		Menu.benvenuto();
		avvio();

	}

	public static void avvio() {
		Menu.opzioni();
		int scelta = 0;

		while (scelta != 7) {
			scelta = ScannerManager.getInt();
			switch (scelta) {
			case 1:
				FantaLista.visualizza();
				Menu.opzioni();
				break;
			case 2:
				Menu.aggiungi();
				FantaLista.aggiungiInLista();
				Menu.opzioni();
				break;
			case 3:
				Menu.rimuovi();
				FantaLista.rimuovi();
				Menu.opzioni();
				break;
			case 4:
				FantaLista.infoDettagliate();
				Menu.opzioni();
				break;
			case 5:
				FantaLista.classifica();
				Menu.opzioni();
				break;
			case 6:
				try {
					FantaLista.compara();
				} catch (MiaEccezione e) {
					System.out.println("I nomi non corrispondono a giocatori presenti in lista,riprova.\n");
				} finally {
					Menu.opzioni();	
				}break;
			case 7:
				Menu.arrivederci();
				break;
			default:
				System.out.println("Operazione non valida,riprova!\n");
				Menu.opzioni();
				break;
			}
		}
	}
}
