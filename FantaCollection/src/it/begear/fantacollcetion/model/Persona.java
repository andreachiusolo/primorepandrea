package it.begear.fantacollcetion.model;

public abstract class Persona {

	private String nome;
	private String cognome;
	private int eta;
	private double altezza;
	private double peso;
	
	public Persona() {
		
	}

	public Persona(String nome, String cognome, int eta, double altezza, double peso) {
		super();
		this.nome = nome;
		this.cognome = cognome;
		this.eta = eta;
		this.altezza = altezza;
		this.peso = peso;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public int getEta() {
		return eta;
	}

	public void setEta(int eta) {
		this.eta = eta;
	}

	public double getAltezza() {
		return altezza;
	}

	public void setAltezza(double altezza) {
		this.altezza = altezza;
	}

	public double getPeso() {
		return peso;
	}

	public void setPeso(double peso) {
		this.peso = peso;
	}

	@Override
	public String toString() {
		return "Nome: " + nome + "  Cognome: " + cognome + "  Età: " + eta + "\nAltezza: " + altezza + "  Peso: "
				+ peso ;
	}


	

}
