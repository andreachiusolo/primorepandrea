package it.begear.fantacollcetion.model;

public class Portiere extends Calciatore {
	private final String ruolo = "Portiere";
	private int golSubiti;

	public Portiere() {
		super();

	}

	public Portiere(String nome, String cognome, int eta, double altezza, double peso, double voto, int golSegnati,
			int golSubiti, int cartelliniGialli, int cartelliniRossi) {
		super(nome, cognome, eta, altezza, peso, voto, golSegnati, cartelliniGialli, cartelliniRossi);

	}

	public int getGolSubiti() {
		return golSubiti;
	}

	public void setGolSubiti(int golSubiti) {
		this.golSubiti = golSubiti;
	}

	public String getRuolo() {
		return ruolo;
	}

	@Override
	public String toString() {
		return super.toString() + "\nGol Subiti: " + golSubiti + "  Ruolo: " + ruolo +  "\n";
	}

	@Override
	public int compareTo(Calciatore c) {
		// TODO Auto-generated method stub
		return super.compareTo(c);
	}

	@Override
	public String InfoRapide() {
		return getNome() + " " + getCognome() + " Età: " + getEta() + " Ruolo: " + getRuolo()
		+ " Voto: " + getVoto()+ "\n";
		
	}

}
