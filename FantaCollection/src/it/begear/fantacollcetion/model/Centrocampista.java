package it.begear.fantacollcetion.model;

public class Centrocampista extends Calciatore{
	private final String ruolo = "Centrocampista";

	public Centrocampista() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Centrocampista(String nome, String cognome, int eta, double altezza, double peso, double voto,
			int golSegnati, int cartelliniGialli, int cartelliniRossi) {
		super(nome, cognome, eta, altezza, peso, voto, golSegnati, cartelliniGialli, cartelliniRossi);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return super.toString() + "\nRuolo: " + ruolo + "\n";
	}

	public String getRuolo() {
		return ruolo;
	}

	@Override
	public int compareTo(Calciatore c) {
		// TODO Auto-generated method stub
		return super.compareTo(c);
	}

	@Override
	public String InfoRapide() {
		return getNome() + " " + getCognome() + " Età: " + getEta() + " Ruolo: " + getRuolo()
		+ " Voto: " + getVoto()+ "\n";
		
	}


	
	

}
