package it.begear.fantacollcetion.model;

public abstract class Calciatore extends Persona implements Comparable<Calciatore> {

	private double voto;
	private int golSegnati;
	private int cartelliniGialli;
	private int cartelliniRossi;

	public Calciatore() {
		super();
	}

	public Calciatore(String nome, String cognome, int eta, double altezza, double peso, double voto, int golSegnati,
			int cartelliniGialli, int cartelliniRossi) {

		super(nome, cognome, eta, altezza, peso);

	}

	public int getCartelliniGialli() {
		return cartelliniGialli;
	}

	public void setCartelliniGialli(int cartelliniGialli) {
		this.cartelliniGialli = cartelliniGialli;
	}

	public int getCartelliniRossi() {
		return cartelliniRossi;
	}

	public void setCartelliniRossi(int cartelliniRossi) {
		this.cartelliniRossi = cartelliniRossi;
	}

	public double getVoto() {
		return voto;
	}

	public void setVoto(double voto) {
		this.voto = voto;
	}

	public int getGolSegnati() {
		return golSegnati;
	}

	public void setGolSegnati(int golSegnati) {
		this.golSegnati = golSegnati;
	}

	@Override
	public String toString() {
		return super.toString() + "\nVoto: " + voto + "\nGol segnati: " + golSegnati + "  Cartellini Gialli: "
				+ cartelliniGialli + " Cartellini Rossi: " + cartelliniRossi;
	}

	@Override
	public int compareTo(Calciatore c) {      // compara i giocatori in base al voto e li classifica in
		if (c.getVoto() == getVoto()) {       // ordine di voto decrescente
			return 0;
		} else if (c.getVoto() < getVoto()) {
			return -1;
		} else {
			return 1;
		}
	}

	public abstract String InfoRapide();

}
