package it.begear.fantacollcetion.model;

public class Difensore extends Calciatore {
	private final String ruolo = "Difensore";

	public Difensore() {
		super();

	}

	public Difensore(String nome, String cognome, int eta, double altezza, double peso, double voto, int golSegnati,
			int cartelliniGialli, int cartelliniRossi) {
		super(nome, cognome, eta, altezza, peso, voto, golSegnati, cartelliniGialli, cartelliniRossi);

	}

	public String getRuolo() {
		return ruolo;
	}

	@Override
	public String toString() {
		return super.toString() + "\nRuolo: " + ruolo + "\n";
	}

	@Override
	public int compareTo(Calciatore c) {
		// TODO Auto-generated method stub
		return super.compareTo(c);
	}

	@Override
	public String InfoRapide() {
		return getNome() + " " + getCognome() + " Età: " + getEta() + " Ruolo: " + getRuolo()
		+ " Voto: " + getVoto()+ "\n";
		
	}

}
