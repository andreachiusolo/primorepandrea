package it.begear.fantacollcetion.repository;

import java.util.ArrayList;
import java.util.Collections;

import it.begear.fantacollcetion.controller.FantaCollection;
import it.begear.fantacollcetion.model.Attaccante;
import it.begear.fantacollcetion.model.Calciatore;
import it.begear.fantacollcetion.model.Centrocampista;
import it.begear.fantacollcetion.model.Difensore;
import it.begear.fantacollcetion.model.Persona;
import it.begear.fantacollcetion.model.Portiere;
import it.begear.fantacollcetion.util.ScannerManager;
import it.begear.fantacollection.exc.MiaEccezione;
import it.begear.fantacollection.service.AttaccanteService;
import it.begear.fantacollection.service.CalciatoreService;
import it.begear.fantacollection.service.CentrocampistaService;
import it.begear.fantacollection.service.DifensoreService;
import it.begear.fantacollection.service.PersonaService;
import it.begear.fantacollection.service.PortiereService;
import it.begear.fantacollection.view.Menu;

public  class FantaLista {

	private static ArrayList<Calciatore> fantalista = new ArrayList<>();

	public ArrayList<Calciatore> getFantalista() {
		return fantalista;
	}

	public static void aggiungiInLista() { // metodo per aggiungere un giocatore alla lista
		int scelta = ScannerManager.getInt();
		switch (scelta) {
		case 1:
			Portiere p = new Portiere();
			p = PortiereService.aggiungi();
			fantalista.add(p);
			break;
		case 2:
			Difensore d = new Difensore();
			d = DifensoreService.aggiungi();
			fantalista.add(d);
			break;
		case 3:
			Centrocampista cc = new Centrocampista();
			cc = CentrocampistaService.aggiungi();
			fantalista.add(cc);
			break;
		case 4:
			Attaccante a = new Attaccante();
			a = AttaccanteService.aggiungi();
			fantalista.add(a);
			break;
		case 5:
			FantaCollection.avvio();
			break;
		default:
			System.out.println("Operazione non valida,riprova!");
			break;

		}
	}

	public static void rimuovi() { // metodo per rimuovere giocatore dalla lista
		System.out.println("Inserisci nome del giocatore da rimuovere:");
		String nome = ScannerManager.getString();
		System.out.println("Inserisci cognome del giocatore:");
		String cognome = ScannerManager.getString();
		try {
			for (Calciatore c : fantalista) {

				if (nome.toLowerCase().equals(c.getNome().toLowerCase())
						&& cognome.toLowerCase().equals(c.getCognome().toLowerCase())) 

					fantalista.remove(c);
				
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("eccezione");
		}

	}

	public static void visualizza() { // metodo per visualizzare intera lista con info rapide
		for (Calciatore c : fantalista) {
			System.out.println(c.InfoRapide());
		}
	}

	public static void infoDettagliate() { // metodo per visualizzare info dettagliate su un preciso giocatore

		System.out.println("Inserisci nome del giocatore di cui vuoi visualizzare le info:");
		String nome = ScannerManager.getString();
		System.out.println("Inserisci cognome del giocatore:");
		String cognome = ScannerManager.getString();
		for (Calciatore c : fantalista) {
			if (nome.toLowerCase().equals(c.getNome().toLowerCase())
					&& cognome.toLowerCase().equals(c.getCognome().toLowerCase())) {
				System.out.println(c);
			} else {
				System.out.println("Giocatore non trovato");
			}
		}
	}

	public static void classifica() { // metodo per classificare i giocatori
		Collections.sort(fantalista);
		for (Calciatore c : fantalista) {
			System.out.println(c.InfoRapide());
		}

	}

	public static void compara() throws MiaEccezione { // metodo per comparare due giocatori scelti da tastiera

		ArrayList<Calciatore> lista_max = new ArrayList<>();
		System.out.println("Inserisci cognome del primo giocatore");
		String cognome_1 = ScannerManager.getString();
		System.out.println("Inserisci cognome del secondo giocatore:");
		String cognome_2 = ScannerManager.getString();
		for (Calciatore c : fantalista) {
			if (cognome_1.toLowerCase().equals(c.getCognome().toLowerCase())
					|| cognome_2.toLowerCase().equals(c.getCognome().toLowerCase())) {
				lista_max.add(c);
			}
		}
		if (lista_max.size() > 1) {
			System.out.println("Il giocatore con il voto maggiore è:\n");
			Collections.sort(lista_max);
			System.out.println(lista_max.get(0).InfoRapide());
			lista_max.clear();
		} else {
			throw new MiaEccezione();
		}
	}

}
