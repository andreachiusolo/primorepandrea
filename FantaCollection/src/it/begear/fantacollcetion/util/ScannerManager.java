package it.begear.fantacollcetion.util;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ScannerManager {

	public static int getInt() {

		Scanner scanner = new Scanner(System.in);
		try {
			return scanner.nextInt();
		} catch (InputMismatchException e) {
			System.out.println("Inserisci un valore corretto!");
			return getInt();
		}
	}

	public static double getDouble() {

		Scanner scanner = new Scanner(System.in);
		if (scanner.hasNextDouble()) {
			return scanner.nextDouble();
		} else
			return getDouble();
	}

	public static String getString() {

		Scanner scanner = new Scanner(System.in);
		try {
			return scanner.nextLine();
		} catch (InputMismatchException e) {
			System.out.println("Inserisci un valore corretto!");
			return getString();
		}
	}

}
