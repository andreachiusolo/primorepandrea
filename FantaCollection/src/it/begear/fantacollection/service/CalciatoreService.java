package it.begear.fantacollection.service;

import it.begear.fantacollcetion.model.Calciatore;
import it.begear.fantacollcetion.model.Persona;
import it.begear.fantacollcetion.util.ScannerManager;

public class CalciatoreService {
	
	public static Calciatore aggiungi(Calciatore c) {      
		c = (Calciatore) new PersonaService().aggiungi(c);
		System.out.println("Inserisci voto:");
		c.setVoto(ScannerManager.getDouble());
		System.out.println("Inserisci n. Goal Segnati:");    
		c.setGolSegnati(ScannerManager.getInt());
		System.out.println("Inserisci n. cartellini gialli:");
		c.setCartelliniGialli(ScannerManager.getInt());
		System.out.println("Inserisci n.cartellini rossi:");
		c.setCartelliniRossi(ScannerManager.getInt());
		return c;
	}

}
