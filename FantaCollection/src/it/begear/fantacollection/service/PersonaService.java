package it.begear.fantacollection.service;

import it.begear.fantacollcetion.model.Persona;
import it.begear.fantacollcetion.util.ScannerManager;

public class PersonaService implements Service {

	public PersonaService() {

	}

	public Persona aggiungi(Persona p) {
		System.out.println("Inserisci nome: ");
		p.setNome(ScannerManager.getString());
		System.out.println("Inserisci cognome:");
		p.setCognome(ScannerManager.getString());
		System.out.println("Inserisci età:");
		p.setEta(ScannerManager.getInt());
		System.out.println("Inserisci altezza (m):");
		p.setAltezza(ScannerManager.getDouble());
		System.out.println("Inserisci peso (Kg):");
		p.setPeso(ScannerManager.getDouble());
		return p;
	}

}
