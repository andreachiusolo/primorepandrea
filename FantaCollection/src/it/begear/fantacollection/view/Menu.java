package it.begear.fantacollection.view;

public class Menu {

	public static void benvenuto() {
		System.out.println("Benvenuto nell'app Fantacalcio");
	}

	public static void arrivederci() {
		System.out.println("Arrivederci!");
	}

	public static void opzioni() {
		System.out.println("Scegli l'opzione che vuoi effettuare\n");
		System.out.println("1. Vedi lista giocatori");
		System.out.println("2. Aggiungi un giocatore in lista");
		System.out.println("3. Rimuovi un giocatore dalla lista");
		System.out.println("4. Visualizza info dettagliate su un giocatore");
		System.out.println("5. Classifica giocatori in base al voto");
		System.out.println("6. Compara due giocatori");
		System.out.println("7. Esci dall'app");
	}

	public static void aggiungi() {
		System.out.println("Indica il ruolo del giocatore che vuoi aggiungere:\n");
		System.out.println("1. Portiere");
		System.out.println("2. Difensore");
		System.out.println("3. Centrocampista");
		System.out.println("4. Attaccante");
		System.out.println("5. Torna al menù principale");
	}

	public static void rimuovi() {
		System.out.println("Scrivi nome e cognome del giocatore che vuoi rimuovere");
	}

	public static void visualizzaInfo() {
		System.out.println("Scrivi nome e cognome del giocatore di cui vuoi visualizzare le info");
	}

}
